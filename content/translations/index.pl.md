---
locale: pl
seo_title_index: string
seo_desc_index: string
seo_title_about: string
seo_desc_about: string
seo_title_clients: string
seo_desc_clients: string
seo_title_contact: string
seo_desc_contact: string
seo_title_photography: string
seo_desc_photography: string
seo_title_ads: string
seo_desc_ads: string
seo_title_all: string
seo_desc_all: string
seo_title_events: string
seo_desc_events: string
seo_title_music: string
seo_desc_music: string
seo_title_promo: string
seo_desc_promo: string
all: Wszystkie
music: Muzyka
company_ads: Reklamy
events: Wydarzenia
promotional: Promocyjne
about: O Nas
home: Strona główna
videos: Filmy
clients: Klienci
contact: Kontakt
photography: Fotografia
showreel: SHOWREEL
trusted_by: Obecni klienci
about_intro: Offbeat motion to studio produkcji video założone przez Rafała Lipińśkiego.
about_main: Oferujemy kompleksową usługę wideo. Od napisania scenariusza przez organizację planu zdjęciowego po montaż, color grading, udźwiękowienie i przygotowanie plików w formatach do dalszej dystrybucji w mediach.  Specjalizujemy się w produkcji spotów reklamowych, filmów promocyjnych oraz teledysków. Realizujemy również dokumentacje z eventów. Po za filmowaniem zajmujemy się tworzeniem fotografii  w zakresie  sesji zdjęciowych oraz relacji z eventów.
about_final: Do każdego projektu podchodzimy indywidualnie. Tworzenie nowych obrazów to nasza pasja.
photography_header: Wybrane fotografie
contact_intro: Nawiążmy współpracę
contact_main: Czy potrzebujesz profesjonalnego filmu? Zadzwoń lub napisz do nas, odpowiemy jak najszybciej się da!
---
