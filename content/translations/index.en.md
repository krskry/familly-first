---
locale: en
seo_title_index: stringen
seo_desc_index: string
seo_title_about: string
seo_desc_about: string
seo_title_clients: string
seo_desc_clients: string
seo_title_contact: string
seo_desc_contact: string
seo_title_photography: string
seo_desc_photography: string
seo_title_ads: string
seo_desc_ads: string
seo_title_all: string
seo_desc_all: string
seo_title_events: string
seo_desc_events: string
seo_title_music: string
seo_desc_music: string
seo_title_promo: string
seo_desc_promo: string
all: All
music: Music
company_ads: Company ads
events: Events
promotional: Promotional
about: About
home: Home
videos: Videos
clients: Clients
contact: Contact
photography: Photography
showreel: SHOWREEL
trusted_by: Trusted by
about_intro: Offbeat motion is a video production studio founded by Rafał Lipiński.
about_main: I am filmmaker based in the Netherlands  work worldwide. I specialize in commercials  music videos  promotional films corporate films and event coverage. I take care of it all - starting from script through arranging a set  ligthing  shooting  editing and color grading. For bigger projects I organize and cooperate with film crew. I don't treat making films as a work  cinematography is my passion  most of my time a spend on constant developing skills. I always follow modern standards.
about_final: Every new project I treat as a challange. My main principle is the originality of each video. Better go and watch my work  you will see that every project is different and uniqe.
photography_header: Some of the recent shots
contact_intro: Lets get in touch
contact_main: Ready to start your next project with us? Give us a call or send us an email and we will get back to you as soon as possible!
---
