---
locale: nl
seo_title_index: stringen
seo_desc_index: string
seo_title_about: string
seo_desc_about: string
seo_title_clients: string
seo_desc_clients: string
seo_title_contact: string
seo_desc_contact: string
seo_title_photography: string
seo_desc_photography: string
seo_title_ads: string
seo_desc_ads: string
seo_title_all: string
seo_desc_all: string
seo_title_events: string
seo_desc_events: string
seo_title_music: string
seo_desc_music: string
seo_title_promo: string
seo_desc_promo: string
all: Allemaal
music: Muziek
company_ads: Bedrijfsadvertenties
events: Evenementen
promotional: Promotionele
about: Over
home: Startpagina
videos: Videos
clients: Klanten
contact: Contact
photography: Fotografie
showreel: SHOWREEL
trusted_by: Vertrouwd door
about_intro: Offbeat motion is een video productie studio opgericht door Rafal Lipinski.
about_main: Als filmmaker ben ik gebaseerd in nederland maar werk wereldwijd. Ik ben gespecialiseerd in muziek video`s, commercials, promotie video`s, evenementen en bedrijfs video’s. Ik zorg voor alles, van script tot volledig geëdite video. Voor grotere projecten werk ik samen met andere film crews. Films maken zie ik niet als mijn werk, maar als een passie. Ik besteed veel tijd aan het verder ontwikkelen van mijn vaardigheden en maak gebruik van de laatste trends. Leder nieuw project behandel ik als een uitdaging.
about_final: Originaliteit staat bij mij hoog in het vaandel, dit kunt u terug zien in mijn portfolio.,
photography_header: Enkele recente schoten
contact_intro: Laten we beginnen
contact_main: Klaar om uw volgende project bij ons te starten? Bel ons of stuur ons een e-mail en wij nemen zo spoedig mogelijk contact met u op!
---
